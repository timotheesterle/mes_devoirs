# Generated by Django 3.0.5 on 2020-05-04 22:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('homework', '0015_auto_20200504_2359'),
    ]

    operations = [
        migrations.CreateModel(
            name='LectureSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weekly_lecture', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='homework.WeeklyLecture')),
            ],
            options={
                'verbose_name': 'Sessions de cours',
            },
        ),
    ]
