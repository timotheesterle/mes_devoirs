from django.contrib import admin
from .models import (
    Group,
    WeeklyLecture,
    Assignment,
    File,
    Student,
    LectureSession,
)

admin.site.register(Group)
admin.site.register(WeeklyLecture)
admin.site.register(Assignment)
admin.site.register(File)
admin.site.register(Student)
admin.site.register(LectureSession)
