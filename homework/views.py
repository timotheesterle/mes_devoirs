from django.shortcuts import render
from django.views.generic import (
    ListView,
    DetailView,
)
from .models import (
    Group,
    WeeklyLecture,
    LectureSession,
    Assignment,
)

class AssignmentListView(ListView):
    model = Assignment
    template_name = 'homework/home.html'
    context_object_name = 'assignments'

class AssignmentDetailView(DetailView):
    model = Assignment
