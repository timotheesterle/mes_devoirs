from django.apps import AppConfig


class HomeworkConfig(AppConfig):
    name = 'homework'
    verbose_name = 'Devoirs'
