from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from django.urls import reverse

class Student(models.Model):
    class Meta:
        verbose_name = 'Élève'
        verbose_name_plural = 'Élèves'
    first_name = models.CharField(max_length=50, verbose_name='Prénom')
    last_name = models.CharField(max_length=50, verbose_name='Nom de famille')
    phone_number = PhoneNumberField(verbose_name='Numéro de téléphone')

class Group(models.Model):
    class Meta:
        verbose_name = 'Groupe'
        verbose_name_plural = 'Groupes'

    level = models.CharField(max_length=100, verbose_name='Niveau')
    label = models.CharField(max_length=100, verbose_name='Libellé')
    students = models.ManyToManyField(Student, verbose_name='Élèves', blank=True)

    def __str__(self):
        return self.level

DAYS_OF_WEEK = (
    (0, 'Lundi'),
    (1, 'Mardi'),
    (2, 'Mercredi'),
    (3, 'Jeudi'),
    (4, 'Vendredi'),
    (5, 'Samedi'),
    (6, 'Dimanche'),
)

class WeeklyLecture(models.Model):
    class Meta:
        verbose_name = 'Cours hebdomadaire'
        verbose_name_plural = 'Cours hebdomadaires'

    weekday = models.CharField(max_length=1, choices=DAYS_OF_WEEK, verbose_name='Jour de la semaine')
    hour_start = models.TimeField(default=timezone.now, verbose_name='Heure de début')
    hour_end = models.TimeField(default=timezone.now, verbose_name='Heure de fin')
    group = models.ForeignKey(Group, on_delete=models.CASCADE, default=1, verbose_name='Classe')
    subject = models.CharField(max_length=100, verbose_name='Matière')

    def __str__(self):
        return f'{self.weekday} {self.hour_start} {self.group}'

class File(models.Model):
    class Meta:
        verbose_name = 'Fichier'
        verbose_name_plural = 'Fichiers'

    path = models.FileField(upload_to='attached_files', verbose_name='Fichier')

    def __str__(self):
        return self.path.name

class LectureSession(models.Model):
    class Meta:
        verbose_name = 'Séance de cours'
        verbose_name_plural = 'Séances de cours'

    weekly_lecture = models.ForeignKey(WeeklyLecture, on_delete=models.CASCADE, verbose_name='Cours hebdomadaire')
    date = models.DateField(default=timezone.now, verbose_name='Date')

    def __str__(self):
        return f'{self.weekly_lecture}, {self.date}'

class Assignment(models.Model):
    class Meta:
        verbose_name = 'Devoir'
        verbose_name_plural = 'Devoirs'

    title = models.CharField(max_length=100, verbose_name='Titre')
    content = models.TextField(verbose_name='Contenu')
    lecture_session = models.ForeignKey(LectureSession, on_delete=models.CASCADE, default=1, verbose_name='Séance de cours')
    due_date = models.DateTimeField(default=timezone.now, verbose_name='À rendre pour le')
    files = models.ManyToManyField(File, verbose_name='Fichiers')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('assignment-detail', kwargs={'pk': self.pk})
