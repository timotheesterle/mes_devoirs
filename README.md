# Mes devoirs

## Description

Cette application est destinée au partage de devoirs par des professeurs de musique.  Elle utilise le framework Django.

Note : l'appli est developpée avec la version 3.8.2 de Python. Si vous souhaitez utiliser plusieurs versions de Python en même temps, il est possible de le faire avec [pyenv](https://github.com/pyenv/pyenv).

## Pour commencer

1. Clonez ce dépôt et déplacez-vous dans le dossier nouvellement créé :

    ```sh
    git clone https://gitlab.com/timotheesterle/mes_devoirs.git
    cd mes_devoirs/
    ```

2. Installez les dépendances :

    ```sh
    pip install -r requirements.txt
    ```

3. Effectuez les migrations

    ```sh
    python manage.py migrate
    ```

4. Lancez le serveur local

    ```sh
    python manage.py runserver
    ```
